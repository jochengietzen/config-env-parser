"""
This module provides logic to expose certain configuration variables as ENV variables for
easy deployment and default configuration
"""
from config_env_parser.iniparser.iniparser import Config
