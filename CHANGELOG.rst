=========
Changelog
=========

Version 0.2
===========

- Automatically write markdown if path is provided, to keep track of your environment variables better


Version 0.1
===========

- Move to pyscaffold structure